package com.andrewswan.bgg4j.xmlapi2.thread;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBException;

import com.andrewswan.bgg4j.impl.JaxbUtils;
import com.andrewswan.bgg4j.xmlapi2.BGGError;

/**
 * This class is a proxy to the calls related to Thread content on BGG.
 * The client should get a instance of this class to search for articles
 * inside a thread. 
 *
 */
public class ThreadCall {
	private static final String BGG_XML_API_BASE = "http://www.boardgamegeek.com/xmlapi2/thread";

	/**
	 * Get one or more individual posts inside a concrete thread that match the criteria, ordered by post time.
	 *  
	 * @param id this is the id of the thread the search is limited to.
	 * @param firstArticle if not null is a lower limit in the article id returned.That is to say, none of the articles returned will have an id lower than this value.
	 * @param firstDate if not null, all the articles returned are from this date or more recent, the precision is of one second. 
	 * @param maxArticles if not null is the maximun number of articles returned.
	 * @param username only articles published by this user will be returned. Note that, as today, this parameter does not has effect.
	 * @return a collection of articles wrapped in a Thread object
	 * @throws BGGError
	 */
	public Thread getThread( int id, Integer firstArticle, Date firstDate, Integer maxArticles, String username ) throws BGGError {
		Thread t;

		t = null;

		URL bggUrl = getRequestURL(id, firstArticle, firstDate, maxArticles, username);
		try {
			t = (Thread) JaxbUtils.getUnmarshaller(Thread.class).unmarshal(bggUrl);
		} catch (final JAXBException e) {
			// Something is wrong with the xml received
			// BGG do not send a thread element when there is a error in his side
			// We check if this is the case.
			try {
				com.andrewswan.bgg4j.xmlapi2.Error err = (com.andrewswan.bgg4j.xmlapi2.Error) JaxbUtils.getUnmarshaller(com.andrewswan.bgg4j.xmlapi2.Error.class).unmarshal(bggUrl);
				throw new BGGError( err );

			} catch (JAXBException e1) {
				throw new IllegalStateException(e);
			}			
		}

		return t;
	}

	/**
	 * Get the url corresponding with the desired parameters.
	 * 
	 * @param id this is the id of the thread the search is limited to.
	 * @param firstArticle if not null is a lower limit in the article id returned.That is to say, none of the articles returned will have an id lower than this value.
	 * @param firstDate if not null, all the articles returned are from this date or more recent, the precision is of one second. 
	 * @param maxArticles if not null is the maximun number of articles returned.
	 * @param username only articles published by this user will be returned. Note that, as today, this parameter does not has effect.
	 * @return the url to call to get the desired response
	 */
	private URL getRequestURL(int id, Integer firstArticle, Date firstDate, Integer maxArticles, String username) {
		URL url;
		StringBuffer sb;
		
		try {
			sb = new StringBuffer();
			sb.append(BGG_XML_API_BASE);
			sb.append("?id=");
			sb.append(id);
			if( firstArticle != null ) {
				sb.append("&minarticleid=");
				sb.append(firstArticle);
			}
			if( firstDate != null ) {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sb.append("&minarticledate=");
				sb.append(df.format(firstDate));
			}	
			if( maxArticles != null ) {
				sb.append("&count=");
				sb.append(maxArticles);
			}
			// NOTE: As today this parameter is not supported by BGG API. Maybe tomorrow.
			if( username != null ) {
				sb.append("&username=");
				sb.append(username);
			}
			url = new URL(sb.toString());
		} catch (final MalformedURLException e) {
			throw new IllegalStateException(e);
		}
		
		return url;
	}
}
