package com.andrewswan.bgg4j.xmlapi2;

/** 
 * BGGError wraps any error produced in the BGG side of the transaction.
 * The message of error is taken from the xml response of the site.
 * 
 */
public class BGGError extends Exception {

	public BGGError(com.andrewswan.bgg4j.xmlapi2.Error err) {
		super(err.getMessage());
	}

}
