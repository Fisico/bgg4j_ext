package com.andrewswan.bgg4j.impl;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.andrewswan.bgg4j.xmlapi2.BGGError;
import com.andrewswan.bgg4j.xmlapi2.thread.ThreadCall;


public class XmlForumTest {

	   // Fixture
    private ThreadCall call;

    @Before
    public void setUp() {
        call = new ThreadCall();
    }

    @Test
    @Ignore("Requires BGG to be up and reachable")
    public void basicUse() throws BGGError {
        assertNotNull( call.getThread(1,null,null,null,null)); 
    }
    
    @Test
    @Ignore("Requires BGG to be up and reachable")
    public void completeUse() throws BGGError {
        assertEquals( call.getThread(1,35000,null,null,null).getArticles().getArticle().size(),5); 
        Calendar c = Calendar.getInstance();
		c.set(2004, 5, 10, 23, 12 , 12);
		assertEquals( call.getThread(1,null,c.getTime(),null,null).getArticles().getArticle().size(),4);
		assertEquals( call.getThread(1,null,null,3,null).getArticles().getArticle().size(),3);
    }
}
